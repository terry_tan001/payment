# 支付工具

#### 介绍
1.合并支付宝与微信的官方支付sdk工具
2.集成laravel框架的Provider与Facades
3.继承了微信官方和支付宝官方sdk，并加以优化，删除繁琐和重复的类。

仓库地址：gitee.com/terry_tan001/payment.git

#### 安装教程
1.  composer require terry_tan/payment_tools

#### 在Laravel中的使用说明

1.  添加服务器提供者至框架 Payment\PaymentProvider::class
2.  发布配置文件 php artisan vendor:publish --provider="Payment\PaymentProvider"
