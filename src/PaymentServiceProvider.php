<?php
namespace Payment;

use Illuminate\Support\ServiceProvider;
use Payment\AliPayment\AliPaymentGenerator;
use Payment\WeChatPayment\WxPayGenerator;

class PaymentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this -> publishes([
            __DIR__ . '/config.php' => config_path('Payment.php')
        ]);
    }

    public function register()
    {
        $this -> app -> singleton('weChatPayment',function($app){
            return new WxPayGenerator($app['config']['Payment']['WxPay']);
        });

        $this -> app -> singleton('aliPayment',function($app){
            return new AliPaymentGenerator($app['config']['Payment']['AliPay']);
        });
    }

}