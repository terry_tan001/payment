<?php


namespace Payment\AliPayment\Facades;


use Illuminate\Support\Facades\Facade;

class AliPayment extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'aliPayment';
    }
}
