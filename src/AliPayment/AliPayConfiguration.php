<?php

namespace Payment\AliPayment;


class AliPayConfiguration
{
    protected $sign_type = 'RSA2';

    protected $alipay_public_key = '';

    protected $merchant_private_key = '';

    protected $charset = 'UTF-8';

    protected $gatewayUrl = 'https://openapi.alipay.com/gateway.do';

    protected $app_id = '';

    protected $notify_url = '';

    protected $MaxQueryRetry = 10;

    protected $QueryDuration = 3;

    protected $logPath = '';

    public function __construct($config)
    {
        foreach ($config as $key => $vl)
        {
            $this -> $key = $vl;
        }
    }

    /**
     * @return string
     */
    public function getSignType(): string
    {
        return $this->sign_type;
    }

    /**
     * @return string
     */
    public function getAlipayPublicKey(): string
    {
        return $this->alipay_public_key;
    }

    /**
     * @return string
     */
    public function getMerchantPrivateKey(): string
    {
        return $this->merchant_private_key;
    }

    /**
     * @return string
     */
    public function getCharset(): string
    {
        return $this->charset;
    }

    /**
     * @return string
     */
    public function getGatewayUrl(): string
    {
        return $this->gatewayUrl;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->app_id;
    }

    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notify_url;
    }

    /**
     * @return int
     */
    public function getMaxQueryRetry(): int
    {
        return $this->MaxQueryRetry;
    }

    /**
     * @return int
     */
    public function getQueryDuration(): int
    {
        return $this->QueryDuration;
    }

    /**
     * @return string
     */
    public function getLogPath(): string
    {
        return $this->logPath;
    }

    /**
     * @param string $logPath
     */
    public function setLogPath(string $logPath): void
    {
        $this->logPath = $logPath;
    }

}
