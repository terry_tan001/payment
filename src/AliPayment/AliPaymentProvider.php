<?php

namespace Payment\AliPayment;

use Illuminate\Support\ServiceProvider;

class AliPaymentProvider extends ServiceProvider
{
    public function boot()
    {
        $this -> publishes([
            __DIR__ .'/config.php' => config_path('aliPayment.php')
        ]);
    }

    public function register()
    {
        $this -> app -> singleton('aliPayment',function($app){
            return new AliPaymentGenerator($app['config']['aliPayment']);
        });
    }
}
