<?php

namespace Payment\WeChatPayment\lib;

use Payment\Log;
use Payment\PaymentException;
use Payment\WeChatPayment\WeChatPaymentConfiguration;

/**
 *
 * 回调基础类
 * @author widyhu
 *
 */
class WxPayNotify extends WxPayNotifyReply
{
	private $config = null;

	private static $callback;

	public function setCallBack(callable $callback)
    {
        self::$callback = $callback;
    }

    /**
     *
     * 回调入口
     * @param WeChatPaymentConfiguration $config 配置信息
     * @param bool $needSign 是否需要签名返回
     */
	final public function Handle(WeChatPaymentConfiguration $config, $needSign = true)
	{
	    if(is_callable(self::$callback) == false) Log::ERROR('未设置回调处理函数');

		$this->config = $config;
		$msg = "OK";
		//当返回false的时候，表示notify中调用NotifyCallBack回调失败获取签名校验失败，此时直接回复失败
		$result = WxPayApi::notify($config, [$this, 'NotifyCallBack'], $msg);
		if($result == false){
			$this->SetReturn_code("FAIL");
			$this->SetReturn_msg($msg);
			$this->ReplyNotify(false);
			return;
		} else {
			//该分支在成功回调到NotifyCallBack方法，处理完成之后流程
			$this->SetReturn_code("SUCCESS");
			$this->SetReturn_msg("OK");
		}
		$this->ReplyNotify($needSign);
	}

	/**
	 *
	 * 回调方法入口，子类可重写该方法
	 	//1、进行参数校验
		//2、进行签名验证
		//3、处理业务逻辑
	 * 注意：
	 * 1、微信回调超时时间为2s，建议用户使用异步处理流程，确认成功之后立刻回复微信服务器
	 * 2、微信服务器在调用失败或者接到回包为非确认包的时候，会发起重试，需确保你的回调是可以重入
	 * @param WxPayNotifyResults $objData 回调解释出的参数
	 * @param WxPayConfigInterface $config
	 * @param string $msg 如果回调处理失败，可以将错误信息输出到该方法
	 * @return bool true 回调出来完成不需要继续回调，false回调处理未完成需要继续回调
	 */
	final public function NotifyProcess(WxPayNotifyResults $results, $config, &$msg)
	{
	    $data = $results -> GetValues();

        try {

            if(isset($data['return_code']) == false ||$data['return_code'] != "SUCCESS") throw new PaymentException("异步通知异常");

            if(isset($data['transaction_id']) == false) throw new PaymentException("异步通知异常 输入的参数不正确");


            if($results->CheckSign($config) == false) throw new PaymentException("异步通知异常 签名错误");

            if(!$this->Queryorder($data["transaction_id"],$config)) throw new PaymentException("异步通知异常 订单查询失败");

        } catch(PaymentException $e) {
            $msg = $e -> getMessage();
            Log::ERROR(json_encode(['msg' => $msg ,'data' => $data]));
            return false;
        }
        Log::DEBUG(json_encode($data));

        call_user_func(self::$callback,$data);

        return true;
	}

	/**
	*
	* 业务可以继承该方法，打印XML方便定位.
	* @param string $xmlData 返回的xml参数
	*
	**/
	public function LogAfterProcess($xmlData)
	{
		return;
	}

	/**
	 *
	 * notify回调方法，该方法中需要赋值需要输出的参数,不可重写
	 * @param WxPayNotifyResults $data
	 * @return bool true回调出来完成不需要继续回调，false回调处理未完成需要继续回调
	 */
	final public function NotifyCallBack(WxPayNotifyResults $data)
	{
		$msg = "OK";

		$result = $this->NotifyProcess($data, $this->config,$msg);

		if($result == true){
			$this->SetReturn_code("SUCCESS");
			$this->SetReturn_msg("OK");
		} else {
			$this->SetReturn_code("FAIL");
			$this->SetReturn_msg($msg);
		}
		return $result;
	}

	/**
	 *
	 * 回复通知
	 * @param bool $needSign 是否需要签名输出
	 */
	final private function ReplyNotify($needSign = true)
	{
		//如果需要签名
		if($needSign == true &&
			$this->GetReturn_code() == "SUCCESS")
		{
			$this->SetSign($this->config);
		}

		$xml = $this->ToXml();
		$this->LogAfterProcess($xml);
		WxpayApi::replyNotify($xml);
	}

    //查询订单
    final public function QueryOrder($transaction_id,WxPayConfigInterface $config)
    {
        $input = new WxPayOrderQuery();
        $input->SetTransaction_id($transaction_id);

        $result = WxPayApi::orderQuery($config, $input);
        if(array_key_exists("return_code", $result)
            && array_key_exists("result_code", $result)
            && $result["return_code"] == "SUCCESS"
            && $result["result_code"] == "SUCCESS")
        {
            return true;
        }
        return false;
    }
}

trait WeChatNotification
{
    /**
     * 异步通知回调业务处理方法
     * @param array $results 通知结果的数据
     * @return void
     */
    abstract public function notification(array $results):void ;

}
