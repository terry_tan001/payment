<?php
namespace Payment\WeChatPayment;


use Payment\WeChatPayment\lib\WxPayConfigInterface;

class WeChatPaymentConfiguration extends WxPayConfigInterface
{
    private $appId = '';

    private $merchantId = '';

    private $notifyUrl = '';

    private $signType = 'HMAC-SHA256';

    private $reportLevel = 1;

    private $key = '';

    private $appSecret = '';

    private $sslCertPath = '';

    private $sslKeyPath = '';

    private $proxyHost = '0.0.0.0';

    private $proxyPort = 0;

    private $logPath = '';

    public function __construct($config)
    {
        foreach ($config as $key => $vl)
        {
            $this -> $key = $vl;
        }
    }

    public function GetAppId()
    {
        return $this -> appId;
    }
    public function GetMerchantId()
    {
        return $this -> merchantId;
    }
    //支付相关配置：支付成功回调地址/签名方式
    public function GetNotifyUrl()
    {
        return $this->notifyUrl;
    }
    public function GetSignType()
    {
        return $this->signType;
    }
    //curl代理设置
    public function GetProxy(&$proxyHost, &$proxyPort)
    {
        $proxyHost = $this->proxyHost;
        $proxyPort = $this->proxyPort;
    }
    //上报信息配置
    public function GetReportLevenl()
    {
        //上报等级，0.关闭上报; 1.仅错误出错上报; 2.全量上报
        return $this->reportLevel;
    }
    //商户密钥信息
    public function GetKey()
    {
        return $this->key;
    }
    public function GetAppSecret()
    {
        return $this->appSecret;
    }
    //证书路径设置
    public function GetSSLCertPath(&$sslCertPath, &$sslKeyPath)
    {
        $sslCertPath = $this->sslCertPath;
        $sslKeyPath = $this->sslKeyPath;
    }

    public function GetLogPath()
    {
        return $this -> logPath;
    }
}
