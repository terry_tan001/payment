<?php
namespace Payment\WeChatPayment;

use Illuminate\Support\ServiceProvider;
class WeChatPaymentProvider extends ServiceProvider
{
    public function boot()
    {
        $this -> publishes([
            __DIR__ .'/config.php' => config_path('weChatPayment.php')
        ]);
    }

    public function register()
    {
        $this -> app -> singleton('weChatPayment',function($app){
            return new WxPayGenerator($app['config']['weChatPayment']);
        });
    }
}
