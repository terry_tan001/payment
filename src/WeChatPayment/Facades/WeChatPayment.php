<?php


namespace Payment\WeChatPayment\Facade;


use Illuminate\Support\Facades\Facade;

class WeChatPayment extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'weChatPayment';
    }

}
